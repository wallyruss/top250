require 'sqlite3'
require 'forwardable'
require_relative 'imdb'
require_relative 'movie'

module Top250
  class DB
    include Top250
    extend Forwardable

    attr_accessor :table_name, :connection

    def_delegators IMDb, :scrape
    def_delegators :connection, :execute
    def_delegators SQLite3::Database, :quote

    def initialize(table_name: TABLE_NAME, table_path: SQLITE_TABLE_PATH)
      @table_name = table_name
      @connection = SQLite3::Database.new(table_path)
    end

    def create
      execute <<-SQL
        CREATE TABLE IF NOT EXISTS #{TABLE_NAME} (
          imdb_id varchar(20) PRIMARY KEY NOT NULL,
          title varchar(255) NOT NULL,
          year int NOT NULL,
          rank int,
          watched int NOT NULL,
          created_at datetime DEFAULT CURRENT_TIMESTAMP
        );
      SQL

      self
    end

    def list_watched
      list(watched: true)
    end

    def list_unwatched
      list(watched: false)
    end

    def list_movies(limit: TOTAL_MOVIES, order: :asc, watched: nil)
      sql_statement = 
        "SELECT * FROM #{TABLE_NAME} WHERE rank <= #{TOTAL_MOVIES}"

      sql_statement +=
        case watched
        when true
          ' AND watched = 1 '
        when false
          ' AND watched = 0 '
        else
          ''
        end

      sql_statement += ' ORDER BY rank '
      sql_statement += order == :asc ? ' ASC ' : ' DESC '
      sql_statement += " LIMIT #{quote(limit.to_s)} "

      execute(sql_statement).map { |raw| Movie.new(*raw) }
    end

    def find_by_title(title)
      execute(
        "SELECT * FROM #{TABLE_NAME} WHERE title = ? LIMIT 1", title
      ).first
    end

    def search_by_title(title)
      execute(
        "SELECT * FROM #{TABLE_NAME} WHERE title LIKE ?", "%#{title}%"
      )
    end

    def insert_movie(movie)
      logger.info("Inserting movie: #{movie.to_s_with_rank}")

      # Insert ignore to add new records
      execute(
        "INSERT OR IGNORE INTO #{TABLE_NAME}
        (imdb_id, title, year, rank, watched, created_at)
        VALUES (?, ?, ?, ?, ?, ?)", movie.to_a
      )

      # Insert update rank (no duplicate key update in sqlite3)
      update_movie_attrs(movie, [:rank, :created_at, :title])
    end

    def save_movie(movie)
      logger.info("Saving movie: #{movie.to_s_with_rank}")
      update_movie_attrs(movie, [:rank, :created_at, :title, :watched])
    end

    def update_list
      with_logging('table update') do
        scrape_output = scrape
        scrape_output.movies.each { |movie| insert_movie(movie) }
        derank_stale_movies(scrape_output.creation_time)
      end

      list_movies
    end

    private

    def derank_stale_movies(time)
      # Remove rank from any old record we didn't just insert
      execute(
        "UPDATE #{TABLE_NAME} SET RANK = NULL WHERE created_at < ?", time.to_s
      )
    end

    def update_movie_attrs(movie, attrs)
      set_sql = "SET " + attrs.map { |attr| "#{attr.to_s} = ?" }.join(', ')
      set_values = attrs.map { |attr| movie.send(attr.to_sym) }

      execute(
        "UPDATE #{TABLE_NAME}
        #{set_sql}
        WHERE imdb_id = ?", [set_values, movie.imdb_id].flatten
      )

      true
    end
  end
end
