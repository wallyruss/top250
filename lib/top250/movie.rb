require 'fuzzy_match'
require 'forwardable'
require_relative '../top250'

module Top250
  class Movie

    MovieNotFoundError = Class.new(StandardError)

    include Top250
    extend  Top250
    extend  Forwardable

    def_delegators :data, :imdb_id, :title, :year
    def_delegators :data, :rank, :watched, :created_at

    class << self
      def find_by_title(title)
        raw_movie = connection.find_by_title(title) || return
        Movie.new(*raw_movie)
      end

      def find_by_title!(title)
        find_by_title(title) || raise(MovieNotFoundError.new(title))
      end

      def search_by_title(title)
        FuzzyMatch.new(connection.movie_list, read: :title).find_all(title)
      end

      def find_and_mark_title_as_watched!(title)
        logger.info("Searching for movie with title: `#{title}`")

        Top250::Movie.find_by_title!(title).tap do |movie|
          movie.watched = true
          movie.save
        end

        logger.info("Successfully marked movie as watched")
      end
      
      def find_and_mark_title_as_unwatched!(title)
        logger.info("Searching for movie with title: `#{title}`")

        Top250::Movie.find_by_title!(title).tap do |movie|
          movie.watched = false
          movie.save
        end

        logger.info("Successfully marked movie as unwatched")
      end
    end

    def initialize(*args)
      self.data = Data.new(*args)
      validate_self
    end

    def watched?
      watched == 1
    end

    def watched=(value)
      data.watched = value ? 1 : 0
    end

    def save
      connection.save_movie(self)
    end

    def to_a
      [imdb_id, title, year, rank, watched, created_at]
    end

    def ==(other)
      other.is_a?(self.class) && imdb_id == other.imdb_id
    end

    def <(other)
      rank > other.rank
    end

    def >(other)
      rank < other.rank
    end

    def <=>(other)
      rank <=> other.rank
    end

    def to_s
      "#{title} (#{year})"
    end

    def to_s_with_rank
      "#{title} (#{year}) - #{rank}"
    end

    private

    Data = Struct.new(:imdb_id, :title, :year, :rank, :watched, :created_at)

    attr_accessor :data

    def validate_self
      validate_imdb_id
      validate_title
      validate_year
      validate_rank
      validate_watched
      validate_created_at
    end

    def invalid_imdb_id?
      imdb_id.nil? || imdb_id !~ /^tt[0-9]+/
    end

    def validate_imdb_id
      raise ArgumentError, "Invalid imdb_id: #{imdb_id}" if invalid_imdb_id?
    end

    def invalid_title?
      !title.is_a?(String) || title.gsub(/\s+/, '').length.zero?
    end

    def validate_title
      raise ArgumentError, "Invalid title: #{title}" if invalid_title?
    end

    def validate_year
      Integer(year)
    rescue ArgumentError
      raise ArgumentError, "Invalid year: #{year}"
    end

    def validate_rank
      Integer(rank)
      raise ArgumentError, "Invalid rank: #{rank}" if rank < 0
    rescue ArgumentError
      raise ArgumentError, "Invalid year: #{rank}"
    end

    def invalid_watched?
      ![0, 1].include?(watched)
    end

    def validate_watched
      raise ArgumentError, "Invalid watched: #{watched}" if invalid_watched?
    end

    def validate_created_at
      Time.strptime(created_at, '%F %T')
    rescue ArgumentError
      raise ArgumentError, "Invalid created_at: #{created_at}"
    end
  end
end
