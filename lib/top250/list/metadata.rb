require 'forwardable'
require_relative '../../top250'

module Top250
  class List
    class Metadata
      include Top250
      extend Forwardable

      attr_accessor :total_watched, :total_unwatched
      attr_accessor :raw_list, :total, :percent_watched

      def_delegators :raw_list, :each

      def initialize(list)
        self.raw_list = list.list
        self.total = raw_list.size

        watched, unwatched = raw_list.partition(&:watched?)

        self.total_watched = watched.size
        self.total_unwatched = unwatched.size
        self.percent_watched = (total_watched / total.to_f * 100).round(2)
      end
    end
  end
end
