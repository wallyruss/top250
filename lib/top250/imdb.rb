require 'nokogiri'
require 'open-uri'
require 'forwardable'
require_relative 'movie'
require_relative '../top250'

module Top250
  module IMDb
    extend Top250

    LINK_BASE_PATH = 'https://www.imdb.com/title/'.freeze

    ScrapeOutput = Struct.new(:creation_time, :movies)

    class << self
      def scrape
        creation_time = Time.now.utc

        # TODO: increase scrape robustness / clean this up
        movies = current_top_html.css('.chart tbody.lister-list tr').each_with_index.map do |movie, i|
          info = movie.children.css('.titleColumn a').first
          link = URI.parse(info['href']).path
          imdb_id = link.gsub(/.*(tt[0-9]+).*/, '\1')
          title = info.children.first.inner_text
          year = movie.children.css('.secondaryInfo').first.inner_text.tr('\)\(', '')

          Movie.new(imdb_id, title, year, i + 1, 0, creation_time.to_s)
        end

        ScrapeOutput.new(creation_time, movies)
      end
    end

    private

    class << self
      def current_top_html
        with_logging("Top #{TOTAL_MOVIES} Scrape") do
          Nokogiri::HTML(open(TOP_250_SOURCE_URL, SCRAPE_HEADERS))
        end
      end
    end
  end
end
