require 'forwardable'
require_relative 'db'
require_relative 'movie'
require_relative 'list/metadata'
require_relative '../top250'

module Top250
  class List
    include Top250
    extend Forwardable

    NO_REGEX   = Regexp.new('^n(o)?')
    YES_REGEX  = Regexp.new('^y(es)?')
    QUIT_REGEX = Regexp.new('^q(uit)?')
    OUTPUT_DIR = "#{__dir__}/../../"

    def_delegator :connection, :list_movies, :to_a
    def_delegator :connection, :list_movies, :list
    def_delegator :connection, :list_watched, :watched_movies
    def_delegator :connection, :list_unwatched, :unwatched_movies
    def_delegators Movie, :find_by_title, :search_by_title

    def initialize(
      table_name: TABLE_NAME,
      table_path: SQLITE_TABLE_PATH, output_dir: OUTPUT_DIR
    )
      self.output_dir = File.expand_path(output_dir)

      connection = DB.new(
        table_name: table_name, table_path: table_path
      ).create
    end

    def update
      connection.update_list.tap do
        to_md
        to_txt
      end
    end

    def create_from_user_input
      update.each do |movie|
        prompt_user(movie)
        while true
          case gets.strip.downcase
          when YES_REGEX
            movie.watched = true
            movie.save
            break
          when NO_REGEX
            movie.watched = false
            movie.save
            break
          when QUIT_REGEX
            return
          else
            prompt_user(movie)
          end
        end
      end
    end

    def find_and_mark_title_as_watched!(title)
      Movie.find_and_mark_title_as_watched!(title)
      to_txt
      to_md
    end

    def find_and_mark_title_as_unwatched!(title)
      Movie.find_and_mark_title_as_unwatched!(title)
      to_txt
      to_md
    end

    def watched
      list(watched: true)
    end

    def unwatched
      list(watched: false)
    end

    def total_watched
      metadata.total_watched
    end

    def total_unwatched
      metadata.total_unwatched
    end

    def percent_watched
      metadata.percent_watched
    end

    def metadata
      Metadata.new(self)
    end

    def random
      current_list = list
      current_list[rand(current_list.size)]
    end

    def random_unwatched
      current_list = list(watched: false)
      current_list[rand(current_list.size)]
    end

    def to_md(path = "#{output_dir}/top#{TOTAL_MOVIES}.md")
      with_logging("write of file `#{path}`") do
        File.open(path, 'w') do |file|
          current = metadata
          file.write("### Your IMDb Top #{TOTAL_MOVIES} List:\n")
          file.write("* Total movies watched: #{current.total_watched} (#{current.percent_watched}%).\n")
          file.write("* This list was last updated on #{Time.now.utc}.\n\n")
          current.each do |movie|
            markdown = "#{movie.to_s}"
            markdown = "~~#{markdown}~~" if movie.watched?
            markdown = "[#{markdown}](#{Top250::IMDb::LINK_BASE_PATH}#{movie.imdb_id})"
            file.write("#{movie.rank}. #{markdown}\n")
          end
        end
        true
      end
    end

    def to_txt(path = "#{output_dir}/top#{TOTAL_MOVIES}.txt")
      with_logging("write of file `#{path}`") do
        File.open(path, 'w') do |file|
          current = metadata
          current.each do |movie|
            file.write("#{'>' if movie.watched?}\t#{movie.to_s}\n")
          end
          file.write("\nTotal movies watched: #{current.total_watched} (#{current.percent_watched}%).\n")
          file.write("This list was last updated on #{Time.now.utc}.\n")
        end
        true
      end
    end

    private

    attr_accessor :output_dir

    def prompt(movie)
      "Have you watched #{movie.to_s.inspect}? [y/n/q]: "
    end

    def prompt_user(movie)
      print prompt(movie)
    end
  end
end
