require 'logger'
require_relative 'top250/db'

module Top250
  TOTAL_MOVIES       = 250
  TABLE_NAME         = 'movies'.freeze
  SQLITE_TABLE_PATH  = "#{__dir__}/../db/movies.sqlite3".freeze
  TOP_250_SOURCE_URL = 'https://www.imdb.com/chart/top'.freeze

  SCRAPE_HEADERS = {
    'Accept-Language' => 'en-US,en;q=0.9',
    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36' 
  }.freeze

  def self.included(base)
    base.extend(self)
  end

  def logger=(value)
    @logger = value
  end

  def logger
    @logger ||= Logger.new(STDOUT)
  end

  def with_logging(action)
    logger.info("Beginning #{action}")
    value = yield
    logger.info("Completed #{action}")
    value
  end

  def connection=(value)
    @connection = value
  end

  def connection
    @connection ||= Top250::DB.new
  end
end
