#!/usr/bin/ruby
folder = File.expand_path('../lib', __dir__)
$:.unshift(folder) unless $:.include?(folder)

require 'optparse'
require 'top250/list'

options = {}
OptionParser.new do |opts|
  opts.banner = 'Usage: top250.rb [options]'
  opts.on('-w', '--watched TITLE', 'Mark movie with given title as watched') do |w|
    options[:watched] = w
  end
  opts.on('-n', '--not-watched TITLE', 'Mark movie with given title as not watched') do |u|
    options[:not_watched] = u
  end
  opts.on('-r', '--random-unwatched', 'Retrieve a random movie that hasn\'t been watched') do
    options[:random] = true
  end
  opts.on('-W', '--list-watched', 'List movies that have been watched') do
    options[:list_watched] = true
  end
  opts.on('-N', '--list-unwatched', 'List movies that have not been watched') do
    options[:list_not_watched] = true
  end
  opts.on('-s', '--summary', 'Generate summary of current list') do
    options[:summary] = true
  end
  opts.on('-u', '--update', 'Update movie list') do
    options[:update] = true
  end
  opts.on('-c', '--create', 'Prompt and create a new list from user input') do
    options[:create] = true
  end
  opts.on('-t', '--table-path PATH', 'Path to SQLite table holding database') do |t|
    options[:table_path] = t
  end
  opts.on('-o', '--output-dir DIRECTORY', 'Directory to output updated txt and md list files') do |o|
    options[:output_dir] = o
  end
  opts.on_tail("-h", "--help", "Prints this help") do
    puts opts
    exit
  end
end.parse!

config = {
  table_path: options[:table_path],
  output_dir: options[:output_dir]
}.keep_if { |_, value| value }

list = Top250::List.new(config)

options.each do |name, value|
  case name
  when :watched
    list.find_and_mark_title_as_watched!(value)
  when :not_watched
    list.find_and_mark_title_as_unwatched!(value)
  when :random
    list.logger.info(
      "Random unwatched movie: #{list.random_unwatched.to_s_with_rank}"
    )
  when :list_watched
    list.watched.each do |movie|
      list.logger.info(movie.to_s_with_rank)
    end
  when :list_not_watched
    list.unwatched.each do |movie|
      list.logger.info(movie.to_s_with_rank)
    end
  when :summary
    metadata = list.metadata
    list.logger.info("Total watched: #{metadata.total_watched}")
    list.logger.info("Total not watched: #{metadata.total_unwatched}")
    list.logger.info("Watched percent: #{metadata.percent_watched}")
  when :update
    list.update
  when :create
    list.create_from_user_input
  end
end
